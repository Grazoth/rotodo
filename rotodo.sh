#!/bin/bash

TODOBIN="/home/julien/bin/todotxt_cli/todo.sh -n -d /home/julien/bin/todotxt_cli/todo.cfg"
TODOFILE="/home/julien/Documents/Clara/encours/todo.txt"
TODOACTION="/home/julien/Documents/Clara/encours/todo_actions.txt"
TODOTEMP="/tmp/todo.txt"

J_ALERTE_CREA_MIN=5
J_ALERTE_CREA_MED=10
J_ALERTE_CREA_MAX=15
J_ALERTE_DUE_MIN=-4
J_ALERTE_DUE_MED=-1
J_ALERTE_DUE_MAX=0

E_ALERTE_CREA_OK="🟢"
E_ALERTE_CREA_MIN="🟡"
E_ALERTE_CREA_MED="🟠"
E_ALERTE_CREA_MAX="🔴"

E_ALERTE_DUE_OK="❇️ "
E_ALERTE_DUE_MIN="⚠️ "
E_ALERTE_DUE_MED="🔥"
E_ALERTE_DUE_MAX="🛑"

E_VISIBILITE="👻"
E_ACTION="✴️ "

# Charge le fichier rotodo.conf qui se trouve dans le dossier de rotodo.sh
if [[ -e $(dirname $(readlink -f $0))/rotodo.conf ]]
then
    source $(dirname $(readlink -f $0))/rotodo.conf
fi

# Charge le fichier .rotodo.conf qui se trouve dans le dossier utilisateur
if [[ -e ${HOME}/.rotodo.conf ]]
then
    source ${HOME}/.rotodo.conf
fi

# Extrait l’ID d’une tâche depuis une chaîne renvoyée par la commande
# ls de todotxt
function _extraire_id_tache {
	id_tache=$(sed -E 's/^([0-9]*).*/\1/' <<<"$1")
}

# Fonction générant une date d’échéance valide à depuis la syntaxe due:+x où x
# est le nb de jours restants
function _gerer_echeance {
	nb_jours=$(sed -E 's/^.*due:\+([0-9]+).*$/\1/' <<<"$1")
	date_echeance=$(date -d "+$nb_jours days" +%Y-%m-%d)
	tache_tmp=$(sed -E "s/due:\+[0-9]+/due:$date_echeance/" <<<$1)
}

function _compter_jours {
	date_ref_s=$(date "+%s" --date $1) # date de base en secondes
	date_auj=$(date --date $(date +%F) "+%s") #date d’aujourd’hui en secondes
	ecart=$(( ($date_auj - $date_ref_s) / (3600*24) )) # écart entre les deux dates en j.
}

# Fonction mettant à jour la date de création d’une tâche en la remplaçant
# par la date du jour
function _maj_date {
    ligne_num=$1
    date_auj=$(date +%Y-%m-%d)
    sed -i -E $ligne_num"s/(\((A|B|C)\) )?([0-9]{4}-[0-9]{2}-[0-9]{2})/\1$date_auj/" $TODOFILE
}

# Teste la chaîne renvoyée par rofi pour vérifier l’une des trois
# situations suivantes : 
# (i) la chaîne commence par un numéro suivi immédiatement d’une lettre : c’est le quick mode. 
# (ii) Un numéro sans lettre collée, c’est une tâche existante
# (iii) Pas de numéro : c’est une nouvelle tâche
function detecte_mode {
	if [[ $1 =~ ^[0-9]+[a-zA-Z]+$ ]]
	then 
		mode="quick"
		_extraire_id_tache "$1"
	elif [[ $1 =~ ^[0-9]+.*$ ]]
	then
		mode="existe"
		_extraire_id_tache "$1"
	else
		mode="nv"
	fi
}
# Création d'une nouvelle tâche avec ajout du contexte @todo si aucun
# contexte n'est spécifié et ajout d’un uuid
function creer_tache {
	tache_tmp=$1

	# Ajout contexte @todo s’il n’y a aucun contexte
	if ! [[ $1 =~ @[[:graph:]]* ]]
	then
		tache_tmp="$tache_tmp @todo"
	fi

	# Gestion échéance format court
	if [[ $1 =~ due:\+[0-9]+ ]]
	then
		_gerer_echeance "$tache_tmp"
	fi

	uuid=$(cat /proc/sys/kernel/random/uuid)
	$TODOBIN -t add "$tache_tmp" "uuid:$uuid"
	notify-send "La tâche a été ajoutée"
}

function modifier_tache {
	case $ret in
		0) $TODOBIN do $id_tache
		   notify-send "La tâche $id_tache est désormais réalisée"
		;;
		10) priorite=$(echo -e "A\nB\nC" | rofi -mesg "Choisissez une priorité" -dmenu -p "Priorité >")
			$TODOBIN pri $id_tache $priorite
			notify-send "La tâche $id_tache a désormais la priorité $priorite"
		;;
		11) contexte=$(echo -e "@backlog\n@todo\n@encours\n@veille" | rofi -dmenu -mesg "Choisissez un contexte" -p "Contexte >")
			sed -i $id_tache"s/@[[:graph:]]*/$contexte/" $TODOFILE
			notify-send "Le contexte de la tâche $id_tache est désormais $contexte"
		;;
		12) nlle_tache=$(rofi -mesg "Modifier votre tâche" -p "Tâche >" -dmenu)
			$TODOBIN replace $id_tache $nlle_tache
			notify-send "La tâche $id_tache a été éditée"
		;;
		13) $TODOBIN -f del $id_tache
			notify-send "La tâche $id_tache a été supprimée"
		;;
		14) ajouter_action $id_tache
		;;
        15) executer_action $id_tache
        ;;
    esac
}

# Extrait un uuid depuis un numéro de tâche et l’affecte à la variable $uuid
function _extraire_uuid {
	sed_commande="${id_tache}p"
    tache_complete=$(sed -n $sed_commande < $TODOFILE)
	uuid=$(sed -E 's/.*uuid:([A-Fa-f0-9]{8}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{12})$/\1/' <<<$tache_complete)
}

function ajouter_action {
	_extraire_uuid $1
	action=$(rofi -dmenu -mesg "Entrez votre action" -p "Action >")
	echo "$uuid☭$action" >> $TODOACTION
	_ajouter_indicateur $id_tache
	notify-send "L’action a bien été ajoutée à la tâche $id_tache"
}

function executer_action {
	_extraire_uuid $1
	action=$(grep "$uuid" $TODOACTION | cut -d "☭" -f2)
	$action
}

# Ajouter l’émoji ✴️  à une tâche possédant une action associée
function _ajouter_indicateur {
	sed -i $1"s/uuid/$E_ACTION uuid/" $TODOFILE
}

# Fonction exécutée lors d’une commande rapide comme 12b ou 32x
function quick_mod_tache {
action=$(sed -E 's/^([0-9]*)(.*)/\2/' <<<$1)
case $action in
	a|A) $TODOBIN pri $id_tache A; notify-send "La tâche $id_tache a désormais la priorité A"
	;;
	b|B) $TODOBIN pri $id_tache B; notify-send "La tâche $id_tache a désormais la priorité B"
	;;
	c|C) $TODOBIN pri $id_tache C; notify-send "La tâche $id_tache a désormais la priorité C"
	;;
	back|backlog|l|log) sed -i $id_tache"s/@[[:graph:]]*/@backlog/" $TODOFILE
	notify-send "La tâche $id_tache est désormais dans le backlog"
	;;
	t|todo) sed -i $id_tache"s/@[[:graph:]]*/@todo/" $TODOFILE
	notify-send "La tâche $id_tache est désormais marquée comme étant à faire"
	;;
	e|encours) sed -i $id_tache"s/@[[:graph:]]*/@encours/" $TODOFILE
	notify-send "La tâche $id_tache est désormais marquée comme étant en cours"
	;;
    maj|auj) _maj_date $id_tache
    notify-send "La tâche $id_tache a été mise à jour avec la date du jour"
    ;;
	v|veille) sed -i $id_tache"s/@[[:graph:]]*/@veille/" $TODOFILE
	notify-send "La tâche $id_tache est désormais marquée comme étant en veille"
	;;
	x|X) $TODOBIN do $id_tache
	notify-send "La tâche $id_tache est désormais réalisée"
	;;
	s|suppr|del|d) $TODOBIN -f del $id_tache
	notify-send "La tâche $id_tache est supprimée"
	;;
	go|g|f|F|faire|ex) executer_action $id_tache
	;;
	aj|ajouter) ajouter_action $id_tache
	;;
esac
}

###########################################################
# Fonction appelée lorsque le script est appelé avec
# l’argument rofi
###########################################################

function mode_rofi {
# Premier appel de rofi, pour lister l'ensemble des tâches existantes
tache=$($TODOBIN -p ls | 
    head -n -2 | 
    sed -E 's/uuid:[A-Fa-f0-9]{8}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{12}//' |
    rofi -dmenu -i -p "Tâche >" \
        -mesg "<b>Entrée</b> > cloturer la tâche sélectionnée ou enregistrer une nouvelle tâche&#x0a;<b>Alt+a</b> > ajouter une action&#x0a;<b>Alt+c</b> > changer le contexte&#x0a;<b>Alt+e</b> > éditer&#x0a;<b>Alt+g</b> > exécuter une action&#x0a;<b>Alt+p</b> > changer la priorité&#x0a;<b>Alt+Suppr</b> > supprimer" \
        -kb-custom-1 "Alt+p" \
        -kb-custom-2 "Alt+c" \
        -kb-custom-3 "Alt+e" \
        -kb-custom-4 "Alt+Delete" \
        -kb-custom-5 "Alt+a" \
        -kb-custom-6 "Alt+g" \
        )
ret=$?
if [[ $ret -eq 1 ]]
then
	exit 0
else
	detecte_mode $tache
	case $mode in
		nv) creer_tache "$tache"
		;;
		existe) modifier_tache "$tache"
		;;
		quick) quick_mod_tache "$tache"
		;;
	esac
fi

mode_emoji

}

###########################################################
# Fonction appelée lorsque le script est appelé avec
# l’argument ls
###########################################################

function mode_ls {
    valeur=$1
    option="-"${valeur:0:1}

    case $valeur in
        @backlog) titre="📎 BACKLOG";;
        @todo) titre="💥 À FAIRE";;
        @encours) titre="⚙️  EN COURS";;
        @veille) titre="⏳ À SURVEILLER";;
        *) titre="$2"
    esac

    if [ $option = "-@" ]
    then
        echo "$titre"
        echo ""
        $TODOBIN $option ls "$valeur" "-$E_VISIBILITE" | head -n -2 | sed -E 's/uuid:[A-Fa-f0-9]{8}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{12}//'
    elif [ $option = "-+" ]
    then
        echo "$titre"
        echo ""
        for i in "@encours" "@todo" "@veille" "@backlog"
        do
            $TODOBIN $option ls $valeur $i -"$E_VISIBILITE" | head -n -2 | sed -E 's/uuid:[A-Fa-f0-9]{8}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{12}//'
        done
    fi
}

function mode_emoji {
    cp $TODOFILE $TODOTEMP

    # Effacement de tous les emojis existants
    sed -i "s/ $E_ALERTE_CREA_OK//" $TODOTEMP
    sed -i "s/ $E_ALERTE_CREA_MIN//" $TODOTEMP
    sed -i "s/ $E_ALERTE_CREA_MED//" $TODOTEMP
    sed -i "s/ $E_ALERTE_CREA_MAX//" $TODOTEMP
    sed -i "s/ $E_ALERTE_DUE_OK//" $TODOTEMP
    sed -i "s/ $E_ALERTE_DUE_MED//" $TODOTEMP
    sed -i "s/ $E_ALERTE_DUE_MIN//" $TODOTEMP
    sed -i "s/ $E_ALERTE_DUE_MAX//" $TODOTEMP
    sed -i "s/ $E_VISIBILITE//" $TODOTEMP

    ligne_num=0
    while read -r line
    do
      ligne_num=$((ligne_num+1))

      # Emojis pour date création
      date_crea=$(sed -E 's/^(\((A|B|C)\) )?([0-9]{4}-[0-9]{2}-[0-9]{2}).*$/\3/' <<<$line)
      _compter_jours $date_crea
      #ecart=$(datediff $date_crea now)

      if [[ $ecart -ge $J_ALERTE_CREA_MAX ]]
      then
         sed -i -E $ligne_num"s/(\((A|B|C)\) )?([0-9]{4}-[0-9]{2}-[0-9]{2})/\1\3 $E_ALERTE_CREA_MAX/" $TODOTEMP
      elif [[ $ecart -ge $J_ALERTE_CREA_MED ]]
      then
         sed -i -E $ligne_num"s/(\((A|B|C)\) )?([0-9]{4}-[0-9]{2}-[0-9]{2})/\1\3 $E_ALERTE_CREA_MED/" $TODOTEMP
      elif [[ $ecart -ge $J_ALERTE_CREA_MIN ]]
      then
         sed -i -E $ligne_num"s/(\((A|B|C)\) )?([0-9]{4}-[0-9]{2}-[0-9]{2})/\1\3 $E_ALERTE_CREA_MIN/" $TODOTEMP
      else
         sed -i -E $ligne_num"s/(\((A|B|C)\) )?([0-9]{4}-[0-9]{2}-[0-9]{2})/\1\3 $E_ALERTE_CREA_OK/" $TODOTEMP
      fi

      # Emojis pour date d’échéance
      # Test de la présence de la sous-chaîne " due:"
      if [[ $line ==  *" due:"* ]]
      then
        date_due=$(sed -E 's/.*due:([0-9]{4}-[0-9]{2}-[0-9]{2}).*/\1/' <<<$line)
        _compter_jours $date_due
        if [[ $ecart -ge $J_ALERTE_DUE_MAX ]]
        then
            sed -i $ligne_num"s/ due:/ $E_ALERTE_DUE_MAX due:/" $TODOTEMP
        elif [[ $ecart -ge $J_ALERTE_DUE_MED ]]
        then
            sed -i $ligne_num"s/ due:/ $E_ALERTE_DUE_MED due:/" $TODOTEMP
        elif [[ $ecart -ge $J_ALERTE_DUE_MIN ]]
        then
            sed -i $ligne_num"s/ due:/ $E_ALERTE_DUE_MIN due:/" $TODOTEMP
        else
            sed -i $ligne_num"s/ due:/ $E_ALERTE_DUE_OK due:/" $TODOTEMP
        fi
                
      fi

      # Emojis pour visibilité. Ajoute l’emoji E_VISIBILITE si la date de visibilité est à venir
      # la mention vis:YYYY-MM-DD
      if [[ $line == *" vis:"* ]]
      then
        date_vis=$(sed -E 's/.*vis:([0-9]{4}-[0-9]{2}-[0-9]{2}).*/\1/' <<<$line)
        _compter_jours $date_vis
        if (( $ecart >= 0 ))
        then
            sed -i -E $ligne_num"s/vis:([0-9]{4}-[0-9]{2}-[0-9]{2})//" $TODOTEMP
        else
            sed -i $ligne_num"s/ vis:/ $E_VISIBILITE vis:/" $TODOTEMP
        fi

      fi


    done < "$TODOTEMP"

    cp $TODOTEMP $TODOFILE
}

case $1 in
    rofi) mode_rofi
    ;;
    ls) mode_ls $2 $3
    ;;
    emoji) mode_emoji
    ;;
esac
