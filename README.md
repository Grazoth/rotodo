# Rotodo

![capture d’écran](img/capture.png)

## Description rapide

Rotodo est un script s’appuyant sur todotxt-cli et simplifiant la gestion de son
flux de tâches, en proposant des outils permettant notamment de :

- changer rapidement le contexte d’une tâche
- associer une action à une tâche
- définir une date de visibilité
- afficher des emojis rendant visibles les tâches urgentes ou anciennes

L’ensemble des opérations s’effectue au clavier, et au clavier seulement.

Pour le tester ou l’utiliser, il faut télécharger le fichier `rotodo.sh` puis
éditer les 4 premières variables :

- `TODOBIN` est l’emplacement de l’exécutable todotxt-cli. Il faut renseigner
  également l’emplacement du fichier de configuration de todotxt-cli (option
  `-d`)
- `TODOFILE` est celui du fichier `todo.txt`
- `TODOACTION` est le fichier contenant les actions associées aux tâches
- `TODOTEMP` est le fichier temporaire sur lequel travaille Rotodo pour
  certaines opérations

Ou vous pouvez créer un fichier `/home/{votre_nom_d’utilisateur}/.rotodo.conf`
ou/et `{dossier_où_se_trouve_rotodo.sh}/rotodo.conf` et y placer les variables
que vous voulez modifier pour surcharger la configuration de base.

Il faut ensuite créer un raccourci clavier exécutant `rotodo.sh rofi`.

## Motivations et prérequis

### Principes de base

À mes yeux, pour être la plus efficace possible, la gestion de son flux de tâche
doit être la plus "transparente" possible, en s’appuyant sur des outils
nécessitant peu d’intervention de l’utilisateur. Rotodo est une tentative de
réponse à cette gageure, puisqu’il suffit de configurer un unique raccourci
clavier pour interagir avec ses tâches et effectuer les différentes actions
mentionnées précédemment.

Cette tentative est une tentative *personnelle* au sens où elle répond
directement à ma façon de fonctionner. Concrètement, l’utilisation de 4
contextes est inscrite "en dur" dans le script :

- `@backlog` est le contexte regroupant les tâches qui ne peuvent pas être
  exécutées immédiatement ;
- `@todo` regroupe les tâches qui peuvent être exécutées immédiatement. Cela
  correspond peu ou prou aux "next actions" de la méthode GTD ;
- `@encours` regroupe les tâches en train d’être traitées ;
- `@veille` regroupe les tâches dont l’exécution est entre les mains d’une
  tierce personne (ex : réponse à un courriel). Ces tâches nécessitent une
  surveillance active (pour une éventuelle relance).

Une tâche n’est donc rattachée qu’à un seul contexte. C’est un usage détourné
des contextes au sens de GTD, puisque cette notion désigne normalement
l’environnement physique et matériel rendant activable ou non une tâche. Les
outils informatiques et Internet rendent à mes yeux quasiment caduques cette
notion initiale du "contexte" (en tout cas dans le métier qui est mien !).

### Dépendance logicielle

Rotodo s’appuie sur `rofi` pour l’interface utilisateur. Il s’agit
donc d’une dépendance obligatoire, tout comme l’est, bien sûr, todotxt-cli.

## Usage

Rotodo s’exécute obligatoirement avec l’un des arguments suivant :

- `rofi` : c’est le "mode" principal, celui qui permet de manipuler ses tâches à
  l’aide de rofi ;
- `ls` : permet de lister ses tâches, en particulier de les lister par contexte
  avec un titre ;
- `emoji` : met à jour l’ensemble des emojis sur de l’ensemble des tâches (par
  exemple, pour passer du vert au jaune une tâche qui a passé un seuil
  d’ancienneté).

### rotodo rofi : manipuler ses tâches

À l’exécution de `rotodo.sh rofi`, toutes les tâches s’affichent. L’utilisateur
a alors le choix entre deux sous-modes : le sous-mode normal et le quick mode.

#### Manipuler ses tâches existantes avec le sous-mode normal

L’utilisateur sélectionne la sous-tâche sur laquelle agir à l’aide des touches
fléchées de son clavier. Il est libre de les filtrer en tapant quelques
caractères. Pour interagir avec la tâche sélectionnée, il lui suffit de taper un
des raccourcis clavier indiqués. Ainsi :

- `Entrée` : marque la tâche comme réalisée. Elle sort donc du fichier
  `todo.txt` pour être archivée dans `done.txt` ;
- `Alt+a` : ajoute une action à la tâche (cf. plus bas). Une nouvelle fenêtre
  rofi s’ouvre pour saisir l’action en question ;
- `alt+c` : permet de changer le contexte de la tâche. Rofi affiche les 4
  contextes possibles, l’utilisateur n’a plus qu’à choisir celui voulu ;
- `alt+g` : exécute l’action associée à une tâche
- `Alt+p` : permet de changer la priorité de la tâche. Rofi affiche les 3
  priorités disponibles, l’utilisateur n’a plus qu’à choisir celle voulue ;
- `Alt+Suppr`: supprime la tâche en cours.

Un appui sur `Échap` permet de quitter sans modification de tâche.

#### Manipuler ses tâches existantes avec le "quick mode"

La première façon de manipuler ses tâches est aisée, mais pas très efficace si
l’on connaît déjà le numéro de la tâche sur laquelle on veut agir. Le "quick
mode" permet d’agir immédiatement sur la tâche voulue. Il consiste simplement à
saisir le numéro de la tâche suivi d’une lettre ou d’une chaîne de caractères
(`12a` ou `23todo` par exemple), parmi les suivantes :

- `a` (ou `A`) : attribue la priorité `A` à la tâche ;
- `aj` (ou `ajouter`) : ajouter une action à la tâche ;
- `b` (ou `B`) : attribue la priorité `B` à la tâche ;
- `c` (ou `C`) : attribue la priorité `C` à la tâche ;
- `d` (ou `del`, `s`, `suppr`) : supprime la tâche ;
- `e` (ou `encours`) : attribue le contexte `@encours` à la tâche ;
- `g` (ou `go`, `f`, `faire`) : exécute l’action associée à la tâche ;
- `l` (ou `log`, `back`, `backlog`) : attribue le contexte `@backlog` à la
  tâche ;
- `t` (ou `todo`) : attribue le contexte `@todo` à la tâche ;
- `v` (ou `veille`) : attribue le contexte `@veille` à la tâche ;
- `x` (ou `X`) : indique la tâche comme étant faite.

Par ailleurs, une action est réalisable dans le quick mode, sans l’être
(pour l’instant) dans le sous-mode normal :

- `maj` (ou `auj`) : modifie la date de création de la tâche pour la date du
  jour ;

#### Créer une nouvelle tâche

La création d’une nouvelle tâche est très simple : après l’exécution de
`rotodo.sh rofi`, il suffit de la saisir puis de presser `Entrée`. La saisie
d’un contexte est facultative : si aucun contexte n’est renseigné, le contexte
`@todo` est automatiquement utilisé. La date de création de la tâche est elle
aussi automatiquement ajoutée.

**Attention** : dans le cas (peu probable) où, après avoir saisi une tâche, rofi
affiche encore une ou des tâches existantes (parce que la saisie
correspond à celle(s)-ci), alors une pression sur `Entrée` a pour effet
de marquer la tâche sélectionnée comme réalisée.

##### Gérer la visibilité d’une tâche

L’utilisateur peut gérer la visibilité d’une tâche en renseignant celle-ci
grâce à la syntaxe `vis:YYYY-MM-DD`. Cela a pour effet de ne pas afficher la
tâche lors de l’utilisation de la commande `rotodo ls` si la date du jour est
antérieure à la date renseignée.

L’intérêt de cette fonctionnalité est de permettre la saisie d’une tâche qui ne
devra pas être exécutée avant un certain laps de temps, sans que celle-ci ne
vienne "parasiter" la liste des tâches à effectuer. Une fois la date indiquée
venue, la tâche devient visible et la mention `vis:…` est automatiquement
supprimée de la tâche.

##### Gérer l’échéance d’une tâche

L’utilisateur peut gérer la date d’échéance d’une tâche (c’est-à-dire la date
limite à laquelle elle doit être réalisée). Deux syntaxes sont possibles : soit
`due:YYYY-MM-DD`, soit `due:+x` où `x` est le nombre de jours séparant de la
date d’échéance.

Cette deuxième syntaxe ne peut être utilisée qu’au moment de la *création*
d’une tâche : c’est en effet cette opération qui traduira la valeur `+x` en
date au format `YYYY-MM-DD`.

#### Gérer les actions associées

Les actions associées visent à aider l’utilisateur à exécuter une tâche en
lançant le logiciel utile pour ce faire. Les actions associées sont renseignées
*après* la création d’une tâche (et ne peuvent donc pas l’être au moment de leur
saisie). Les actions associées consistent en une commande qui sera exécutée
lorsque l’utilisateur le demandera.

Une tâche possédant une action associée est marquée d’un emoji particulier (par
défaut : ✴️ )

Exemple : imaginons que j’ai pour tâche "Lire le journal Le Monde", et que
celle-ci ait pour numéro d’identification `24`. Pour ajouter une action
associée, il suffit de saisir `24aj` puis, dans la boîte de saisie qui s’affiche
alors, de taper la commande `firefox https://lemonde.fr`. Losque je serai prêt à
exécuter cette tâche, je n’aurai qu’à saisir `24g` pour que s’ouvre la page web
du Monde (attention : le numéro d’identification aura toutefois pu changer entre
temps).

À titre personnel, j’utilise beaucoup cette fonctionnalité pour surveiller
l’évolution d’un sondage sur Framadate, ou encore pour pointer vers une page de
mon dokuwiki.

### rotodo ls : lister ses tâches

Les tâches peuvent bien sûr être listées "classiquement" à l’aide de
`todotxt-cli`. Utiliser Rotodo présente toutefois quelques avantage :

- les tâches qui doivent être invisibles le sont effectivement ;
- l’uuid des tâches est caché (l’uuid est utilisé pour les actions associées) ;
- on peut renseigner un "titre" à la liste ;
- lors de la génération d’une liste de tâches par projet, le projet n’apparaît
  pas sur chacune des tâches ;
- idem pour les contextes.

#### Lister ses tâches par contexte

Il suffit d’exécuter la commande `rotodo.sh ls @contexte` où `contexte` est l’un
des quatre contextes parmi : `backlog`, `todo`, `encours` et `veille`. Le titre
de la liste est automatiquement ajouté à celle-ci :

- pour `@backlog` : 📎 BACKLOG
- pour `@todo` : 💥 À FAIRE
- pour `@encours` : ⚙️  EN COURS
- pour `@veille` : ⏳ À SURVEILLER

#### Lister ses tâches par projet

La commande `rotodo.sh ls +projet titre_projet` doit être utilisée.

#### Afficher une liste continuellement à jour de ses tâches

Rotodo ne gère pas l’affiche en continu de liste de tâches à jour. Pour ce
faire, l’utilisation de `watch` est pertinente. Par exemple :

`watch -n 5 -c -t ~/chemin/vers/rotodo.sh ls @todo`

### rotodo emoji : mettre à jour les indicateurs visuels

Rotodo utilise plusieurs types d’indicateurs visuels :

- des pastilles vertes, jaune, orange et rouge pour renseigner sur l’ancienneté
  d’une tâche (c’est-à-dire la durée entre sa date de création et la date du
  jour) ;
- quatre emojis pour renseigner sur la proximité de l’échéance ;
- un emoji "fantôme" appliqué aux tâches devant être invisibles ;
- un emoji renseignant sur l’existance d’une action attachée à la tâche (cf.
  plus haut).

Tous ces indicateurs sont inscrits "en dur" dans le fichier `todo.txt`, ce qui
permet de les utiliser facilement dans d’autres applications (par exemple : on
peut lister les tâches invisibles en filtrant sur l’émoji fantôme).

Après l’exécution de la plupart des opérations pouvant être effectuées avec
`rotodo rofi`, ces indicateurs sont mis à jour. Il est toutefois possible de
lancer cette mise à jour manuellement avec la commande `rotodo.sh emoji`. Ce
peut être utile pour mettre à jour les indicateurs après une modification
externe du fichier `todo.txt`, ou encore pour lancer cette mise à jour au
démarrage de la session utilisateur, par exemple.

## Conseils et astuces

### Rendre ses listes de tâches très facilement accessibles

La visualisation rapide et aisée de ses listes de tâches contribue à
l’utilisation "transparente" de l’outil mentionnée plus haut. Pour ma part,
utilisant le gestionnaire de fenêtres [i3](https://i3wm.org/), je réserve un
*workspace* à l’affichage des quatre contextes. Ce *workspace* est ainsi divisé
en 4 parties, chacune faisant tourner un émulateur de terminal exécutant la
commande `watch` proposée plus haut.

Celles et ceux qui n’utilisent pas un *tiling manager* comme i3 peuvent songer à
l’utilisation d’un screen ou tmux, qui permettent d’afficher plusieurs fenêtres.

Outre que cela permet d’avoir en tête les tâches devant être effectuées dans la
journée ou la semaine, un tel affichage permet d’identifier facilement les
tâches par leur numéro d’identification et donc d’utiliser le quick mode.
À l’inverse, j’utilise le mode normal quand je ne veux pas quitter des yeux
l’application en cours : le filtrage "à la volée" des tâches effectué par rofi
permet d’agir facilement sur celles-ci.

### Prêter attention au numéro d’identification d’une tâche

Le numéro d’identification d’une tâche correspond à son numéro de ligne dans le
fichier `todo.txt`. Lorsqu’une tâche est exécutée ou supprimée, alors le numéro
d’identification des tâches suivantes est logiquement décrémenté de 1. Dans le
cas d’une utilisation rapide et répétée du quick mode, il faut garder cela en
tête pour ne pas saisir de mauvais numéros de tâches, ceux-ci pouvant évoluer
au fur et à mesure des actions effectuées.

### À quelle tâche s’adonner ?

Aucun logiciel ne peut bien sûr choisir pour vous la tâche à effectuer en
priorité. Cela dépend d’un grand nombre de facteurs : l’urgence de la tâche,
son importance, l’envie, la possibilité matérielle de s’en occuper… Pour ma
part, voici approximativement la façon dont je procède :

- en premier lieu, j’observe les indicateurs d’échéance des tâches. Si ces
  indicateurs sont "brûlants", alors je concentre mes efforts sur ces tâches.
  Toutefois, je n’abuse pas des dates d’échéance : je réserve leur mention aux
  tâches qui me semblent vraiment importantes à réaliser en un temps contraint.
  L’abus de mention de dates d’échéance fait en effet courir le risque de noyer
  ces tâches parmi les autres ;
- ensuite, s’il n’y a pas d’indicateur d’échéance "brûlant", j’observe les
  tâches qui sont en haut de listes (surtout dans les contextes `@todo` et
  `@veille`). Je m’adonne alors à celle que j’ai envie de traiter ;
- si, pour une raison ou une autre (manque de temps, manque d’énergie, etc.) je
  ne traite pas l’une de ces tâches, alors je prends le temps d’observer les
  tâches du contexte `@backlog` pour me demander si (i) elles sont complètes et
  (ii) certaines ne mériteraient pas d’être basculées vers `@todo`.

Les indicateurs de couleur renseignant sur l’ancienneté d’une tâche ne doivent pas, à mon sens, surdéterminer le choix de la tâche à traiter : il n’est pas nécessairement grave qu’une tâche soit ancienne (elle peut l’être sans être urgente à traiter). Toutefois, ces indicateurs incitent à se poser quelques bonnes questions : si une tâche du contexte `@todo` est rouge depuis longtemps, est-ce que ça ne signifie pas que je dois simplement l’abandonner ? Ou bien la déléguer ? (ce sont des questions que nous incitent à poser la méthode GTD) Une tâche rouge dans le contexte `@veille` n’indique-t-elle pas que je dois relancer quelqu’un ?

[jln](https://mastodon.zaclys.com/@jln)
